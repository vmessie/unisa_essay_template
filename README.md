# UniSa essay template

This latex package aims to provide a template for making essays.

It was originnaly designed for use within the University of South Australia (UniSa), but using it for other universities should not 
be a problem. 

## Usage
Simply compile with xelatex the file `report.tex` for an exemple

## Acknowledgments

The design of this essay was originally inspired by a free essay latex template that can be found online [here](https://www.latextemplates.com/cat/essays)