\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{UniSA-essay}
\LoadClass[12pt]{article}

\RequirePackage{geometry} % Required to change the page size to A4
\geometry{a4paper} % Set the page size to be A4 as opposed to the default US Letter

\RequirePackage{graphicx} % For including figure

\RequirePackage{float} % Allows more flexibility about figures placement
\RequirePackage{wrapfig} % Allows in-line images

\RequirePackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\RequirePackage{fontspec} % Used for setting the font

\RequirePackage{hyperref} % Used for making clickable links
\RequirePackage{xcolor} % Setting a few common collors

\hypersetup{%
  colorlinks=false,% hyperlinks will be black
  hidelinks
}

\RequirePackage{fancyhdr} % Used to make headers and footers

\pagestyle{fancy}
\fancyhf{}


\setmainfont{Arial} % Main document font

\linespread{2} % Line spacing

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here


\RequirePackage{natbib}  %For making the bibliography
\bibliographystyle{agsm} % havard style bibliography
